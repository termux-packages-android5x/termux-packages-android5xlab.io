module.exports = {
  title: 'My Site',
  tagline: 'The tagline of my site',
  url: 'https://termux-packages-abdroid5x.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  themeConfig: {
    navbar: {
      title: 'My Site',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/termux-packages-android5x',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Community',
          items: [
            {
              label: 'Telegram',
              href: 'https://t.me/joinchat/H6nouPAuCNKo34-c',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/termux-packages-android5x',
            },
              label: 'Packages for Android 7.x+'
              href: "https://github.com/termux/packages"
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} The Pins Team. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-bootstrap',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/termux-packages-android5x/termux-packages-abdroid5x.gitlab.io/edit/master/website',
        },
      },
    ],
  ],
};
